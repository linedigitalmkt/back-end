export const environment = {
    server: { port: process.env.PORT || 3333 },
    db: { url: process.env.DB || 'mongodb://localhost/line-digital' },
    serguranca: { saltRounds: process.env.SALT_ROUNDS || 10, secret: process.env.SECRET || 'dd089fe78bb184336c414c9dea4d16b8' },
    token: { tempo: process.env.TEMPO || 86400 },
    master: { token: process.env.TOKEN || "CWK15484C" }
}